package com.example.lab_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.lang.Exception
import kotlin.math.round

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun ButtonOnClick(v: View){
        var button: Button = findViewById(v.id)
        var value: String = button.text.toString()

        when (value){
            "/", "*", "-", "+" -> {
                AddToOperation(value)
                //Calculate()
            }
            "BACK" -> DeleteLastSymbol()
            "AC" -> ClearAll()
            "=" -> {
                var text_result = findViewById(R.id.text_result) as TextView
                text_result.text = ""

                var text_operation = findViewById(R.id.text_operation) as TextView
                var result = Calculate(text_operation.text.toString())

                if (text_result.text.length != 0) {}
                else if (result - round(result) == 0f)
                    text_result.text = result.toInt().toString()
                else
                    text_result.text = result.toString()
            }
            else -> AddToOperation(value)
        }
    }

    fun AddToOperation(str: String){
        var text_operation = findViewById(R.id.text_operation) as TextView
        var text_result = findViewById(R.id.text_result) as TextView

        if (text_result.text.length > 0 && text_result.text != "Err" && ("+-*/)".contains(str))) // нажали /*-+ имея результат предыдущих вычислений
            text_operation.text = text_result.text.toString() + str
        else if ((text_operation.text.toString() == "0") && ("+-*/)".contains(str))) // нельзя начинать с /*-+)
            Alarm()
        else if ((text_operation.text.toString() == "0") && (str != ".")) // если начинаем с . то 0 оставляем
            text_operation.text = str
        else if ((str == "(") && !("(+-*/".contains(text_operation.text.last()))) // ( может быть только после (/*-+
            Alarm()
        else if ((str == ")") && !(")0123456789".contains(text_operation.text.last()))) // ) может быть только после )0123456789
            Alarm()
        else text_operation.append(str)
    }

    fun DeleteLastSymbol(){
        var text_operation = findViewById(R.id.text_operation) as TextView

        if (text_operation.length() == 1)
            text_operation.text = "0"
        else text_operation.text = text_operation.text.dropLast(1)
    }

    fun ClearAll(){
        var text_operation = findViewById(R.id.text_operation) as TextView
        var text_result = findViewById(R.id.text_result) as TextView

        text_operation.text = "0"
        text_result.text = ""
    }

    fun Calculate(inputStr: String): Float{
        var text_result = findViewById(R.id.text_result) as TextView

        if (text_result.text.length != 0)
            return 0f
        else if (inputStr.isBlank())
            return 0f

        try {
            // на входе число
            var result = inputStr.toFloatOrNull()

            if (result != null)
                return result

            // на входе выражение
            var inputList: MutableList<String> = ArrayList()
            var tmpStr: String = ""
            var backetLevel: Int = 0

            for (ch in inputStr){
                if (backetLevel > 1 && ch == ')'){
                    backetLevel -= 1
                    tmpStr = tmpStr + ch}
                else if (backetLevel == 1 && ch == ')'){
                    backetLevel = 0
                    inputList.add(tmpStr + ch)
                    tmpStr = ""}
                else if (ch == '('){
                    backetLevel += 1
                    tmpStr = tmpStr + ch}
                else if (backetLevel != 0)
                    tmpStr = tmpStr + ch
                else if ("/*-+".contains(ch)){
                    if (tmpStr.isNotEmpty())
                        inputList.add(tmpStr)
                    inputList.add(ch.toString())
                    tmpStr = "" }
                else tmpStr = tmpStr + ch
            }

            if (tmpStr.isNotEmpty())
                inputList.add(tmpStr)

            if ((inputList.count() == 1) && (inputList[0][0] == '(') && (inputList[0][inputList[0].length - 1] == ')'))
                return Calculate(inputList[0].substring(startIndex = 1, endIndex = inputList[0].length - 1))

            var operator: String = ""
            var operatorIndex: Int = 0
            for (i in inputList.indices)
                if ("/*-+".contains(inputList[i]))
                    if (operatorIndex == 0 || (!"/*".contains(operator) && "/*".contains(inputList[i]))){
                        operator = inputList[i]
                        operatorIndex = i}

            var operationResult: Float = 0f
            when (operator){
                "*" -> operationResult = Calculate(inputList[operatorIndex - 1]) * Calculate(inputList[operatorIndex + 1])
                "/" -> operationResult = Calculate(inputList[operatorIndex - 1]) / Calculate(inputList[operatorIndex + 1])
                "+" -> operationResult = Calculate(inputList[operatorIndex - 1]) + Calculate(inputList[operatorIndex + 1])
                "-" -> operationResult = Calculate(inputList[operatorIndex - 1]) - Calculate(inputList[operatorIndex + 1])
            }

            inputList[operatorIndex - 1] = ""
            inputList[operatorIndex] = operationResult.toString()
            inputList[operatorIndex + 1] = ""

            return Calculate(inputList.joinToString(separator = ""))
        } catch (e: Exception) {
            text_result.text = "Err"
            return 0f
        }
    }

    fun Alarm(){
        var text_operation = findViewById(R.id.text_operation) as TextView

        text_operation.setBackgroundResource(R.color.red)
        Thread.sleep(50)
        text_operation.setBackgroundResource(R.color.black)
    }
}